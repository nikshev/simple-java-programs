package com.genstar.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.genstar.xz.CrossXz;

@WebService
public class CrossWS{

	//DI via Spring
	CrossXz crossXz;

	@WebMethod(exclude=true)
	public void setCrossXz(CrossXz crossXz) {
		this.crossXz = crossXz;
	}


	@WebMethod(operationName="getCrossByOem")
	public String getCrossByOem(String oem) {

		return crossXz.getCrossByOem(oem);

	}
}
