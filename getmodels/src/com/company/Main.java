package com.company;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.io.File;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


import javax.imageio.ImageIO;

public class Main {

    public static void main(String[] args) {
	 // write your code here
        ParseCars();
        ParseTrucks();
        ParseCommercial();
    }

    /**
     * Parse cars images and save it to out folder
     */
    private static void ParseCars(){
      BufferedImage image = null;
      for (int i=0;i<cars.length;i++){
         String cars_url=url_for_cars.replace("%car%",cars[i]);
         System.setProperty("http.proxyHost", "192.168.0.1");
         System.setProperty("http.proxyPort", "3128");
         try {
             Document doc = Jsoup.connect(cars_url).get();
             // Get all img tags
             Elements img = doc.getElementsByTag("img");
             // Loop through img tags
             for (Element el : img) {
                 // If alt is empty or null, add one to counter
                 if (!(el.attr("lazyurl").equals("") || el.attr("lazyurl") == null)) {
                     String url_src="http:"+el.attr("lazyurl");
                     int start=url_src.indexOf("Key=")+4;
                     int end=url_src.indexOf("&");
                     String base_name = url_src.substring(start,end);
                     String output_file_name = "/home/eugene/model/" + base_name + ".png";
                     if (url_src.contains("Key=")) {
                         URL url = new URL(url_for_download_exist.replace("%model%",base_name));
                         image = ImageIO.read(url);
                         ImageIO.write(image, "png", new File(output_file_name));
                         System.out.println(output_file_name + " saved...");
                     }
                 }
             }
         }catch (Exception ex){
             ex.printStackTrace();
         }
      }
    }

    /**
     * Parse trucks images and save it to out folder
     */
    private static void ParseTrucks(){
        BufferedImage image = null;
        for (int i=0;i<trucks.length;i++){
            String cars_url=url_for_trucks.replace("%car%",trucks[i]);
            System.setProperty("http.proxyHost", "192.168.0.1");
            System.setProperty("http.proxyPort", "3128");
            try {
                Document doc = Jsoup.connect(cars_url).get();
                // Get all img tags
                Elements img = doc.getElementsByTag("img");
                // Loop through img tags
                for (Element el : img) {
                    // If alt is empty or null, add one to counter
                    if (!(el.attr("lazyurl").equals("") || el.attr("lazyurl") == null)) {
                        String url_src="http:"+el.attr("lazyurl");
                        int start=url_src.indexOf("Key=")+4;
                        int end=url_src.indexOf("&");
                        String base_name = url_src.substring(start,end);
                        String output_file_name = "/home/eugene/model/" + base_name + ".png";
                        if (url_src.contains("Key=")) {
                            URL url = new URL(url_for_download_exist.replace("%model%",base_name));
                            image = ImageIO.read(url);
                            ImageIO.write(image, "png", new File(output_file_name));
                            System.out.println(output_file_name + " saved...");
                        }
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    /**
     * Parse commercial images and save it to out folder
     */
    private static void ParseCommercial(){
        BufferedImage image = null;
        for (int i=0;i<commercial.length;i++){
            String cars_url=url_for_commercial.replace("%car%",commercial[i]);
            System.setProperty("http.proxyHost", "192.168.0.1");
            System.setProperty("http.proxyPort", "3128");
            try {
                Document doc = Jsoup.connect(cars_url).get();
                // Get all img tags
                Elements img = doc.getElementsByTag("img");
                // Loop through img tags
                for (Element el : img) {
                    // If alt is empty or null, add one to counter
                    if (!(el.attr("lazyurl").equals("") || el.attr("lazyurl") == null)) {
                        String url_src="http:"+el.attr("lazyurl");
                        int start=url_src.indexOf("Key=")+4;
                        int end=url_src.indexOf("&");
                        String base_name = url_src.substring(start,end);
                        String output_file_name = "/home/eugene/model/" + base_name + ".png";
                        if (url_src.contains("Key=")) {
                            URL url = new URL(url_for_download_exist.replace("%model%",base_name));
                            image = ImageIO.read(url);
                            ImageIO.write(image, "png", new File(output_file_name));
                            System.out.println(output_file_name + " saved...");
                        }
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }


    //Instance variables
    private static String cars[]={
            "Acura","Alfa_Romeo","Aston_Martin","Audi","Bentley","BMW", "Brilliance","Buick","BYD","Cadillac","Chery","Chevrolet",
            "Chrysler","Citroen","Dacia","Daewoo","DAF","Daihatsu","Dodge","Ferrari","Fiat","Ford","Ford_Asia", "Ford_USA","Foton",
            "Geely", "GMC", "Great_Wall","Hafei","Honda","Hummer","Hyundai","Infiniti","Isuzu","JAC","Jaguar","Jeep","Kia","KTM",
            "Lancia","Land_Rover","Lexus","Lifan","Lincoln","Maruti","Maruti_Suzuki","Maserati","Maybach","Mazda","Mercedes","MG","MINI",
            "Mitsubishi","Nissan","Opel","Peugeot","Piaggio","Pontiac","Porsche","Proton","Renault","Rolls-Royce","Rover","Saab",
            "Seat","Skoda","Smart","Ssangyong","Subaru","Suzuki","Toyota","Vauxhall","Volkswagen","Volvo","ВАЗ","ГАЗ","ЗАЗ","Москвич","УАЗ"};
    private static String trucks[]={
            "Avia","DAF","Fiat","Ford","Hyundai","Isuzu","Iveco","MAN","Mercedes","Mitsubishi","Neoplan","Nissan",
            "Renault_Trucks","Scania","Setra","Van_Hool","Volkswagen","Volvo","КАвЗ","КАМАЗ","МАЗ","МАЗ-MAN"};
    private static String commercial[]={
            "Alfa_Romeo","Avia","Chevrolet","Citroen","Daewoo","DAF","Daihatsu","Dodge","Fiat","Ford","Ford_Asia",
            "Ford_USA","Foton","GMC","Hafei","Honda","Hyundai","Isuzu","Iveco","Kia","LDV","Mazda","Mercedes","Mitsubishi",
            "Nissan","Opel","Peugeot","Piaggio","Renault","Renault_Trucks","Ssangyong","Suzuki","Toyota","Vauxhall","Volkswagen","ГАЗ"
    };


    private static String url_for_download_exist="http://img.exist.ru/img.jpg?Key=%model%&Size=350x350&MethodType=5";
    private static String url_for_cars="http://exist.ua/cat/TecDoc/Cars/%car%?all=1";
    private static String url_for_trucks="http://exist.ua/cat/TecDoc/Trucks/%car%?all=1";
    private static String url_for_commercial="http://exist.ua/cat/TecDoc/Commercial/%car%?all=1";

}
